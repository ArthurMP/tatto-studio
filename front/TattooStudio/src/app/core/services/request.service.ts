import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable()
export class RequestService {

  constructor(
    private http: HttpClient
  ) { }

  get = <T>(endpoint: string): Observable<T> => this.http.get<T>(endpoint).pipe(take(1));

  getById = <T>(endpoint: string, id: number): Observable<T> => this.http.get<T>(`${endpoint}/${id}`).pipe(take(1));

  post = <T>(endpoint: string, body: T): Observable<T> => this.http.post<T>(this.buildUrl(endpoint), body).pipe(take(1));

  put = <T>(endpoint: string, id: string, body: T): Observable<T> =>
    this.http.put<T>(`${endpoint}/${id}`, body).pipe(take(1));

  delete = (endpoint: string, id: string): Observable<void> =>
    this.http.delete<void>(`${endpoint}/${id}`).pipe(take(1));

  private buildUrl = (endpoint: string): string => {
    return `http://ce6e936204e4.ngrok.io/api/public/${endpoint}`
  }
}
