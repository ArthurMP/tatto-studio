import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { RequestService } from 'src/app/core/services/request.service';
import { FormService } from './services/form.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  showPassword: boolean = false;

  constructor(
    private router: Router,
    public formService: FormService,
    private requestService: RequestService,
    private toastController: ToastController,
  ) { }

  ngOnInit() { 
    this.formService.generateform();
  }

  togglePasswordVisibility = (): void => {
    this.showPassword = !this.showPassword;
  }

  navigate = (route: string): void => {
    this.router.navigateByUrl(route);
  }

  login = (): void => {
    if (this.formService.form.valid) {
      this.requestService.post('login', this.formService.form.getRawValue()).subscribe(() => {
        this.presentToast('Login efetuado com sucesso', 'success');
      }, err => {
        this.presentToast(
          err?.error ? err?.error?.errors[0]?.message : 'Ocorreu um erro no processo de login',
          'danger');
      });
    }
  }

  private async presentToast(message: string, color?: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
    });

    toast.present();
  }
}
