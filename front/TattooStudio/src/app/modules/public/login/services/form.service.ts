import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class FormService {

  private loginForm: FormGroup;

  get form(): FormGroup {
    return this.loginForm;
  }

  constructor(
    private formBuilder: FormBuilder
  ) { }

  generateform = () => {
    this.loginForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }
}
