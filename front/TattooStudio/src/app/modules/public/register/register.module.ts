import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { FormPageComponent } from './form-page/form-page.component';
import { FormService } from './form-page/services/form/form.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [RegisterComponent, FormPageComponent],
  imports: [CommonModule, RegisterRoutingModule, ReactiveFormsModule, FormsModule, IonicModule],
  providers: [FormService]
})
export class RegisterModule { }
