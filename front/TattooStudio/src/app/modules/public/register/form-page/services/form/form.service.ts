import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class FormService {

  private registerForm: FormGroup;

  get form(): FormGroup {
    return this.registerForm;
  }

  constructor(
    private formBuilder: FormBuilder
  ) { }

  generateForm = (): void => {
    this.registerForm = this.formBuilder.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      password: [null, Validators.required],
      passwordConfirmation: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      type: [null, Validators.required],
    });
  }
}
