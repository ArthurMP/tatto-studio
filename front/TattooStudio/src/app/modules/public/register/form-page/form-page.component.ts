import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { RequestService } from 'src/app/core/services/request.service';
import { FormService } from './services/form/form.service';

@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.scss'],
})
export class FormPageComponent implements OnInit {

  title: string;
  showPassword: boolean = false;
  showConfirmPassword: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private requestService: RequestService,
    private toastController: ToastController,
    public formService: FormService,
  ) { }

  ngOnInit() {
    this.formService.generateForm();
    this.title = this.activatedRoute.snapshot.params.type === 'CLIENTE' ? 'Cliente' : 'Tatuador';
    this.formService.form.get('type').setValue(
      this.activatedRoute.snapshot.params.type === 'CLIENTE' ? 'CLIENT' : 'EMPLOYEE');
  }

  goBack = (): void => {
    this.router.navigateByUrl('public/register');
  }

  submit = (): void => {
    if (this.formService.form.valid) {
      this.requestService.post('signup', this.formService.form.getRawValue()).subscribe(() => {
        this.presentToast('Usuário cadastrado com sucesso', 'success');
      }, err => {
        this.presentToast(
          err?.error ? err?.error?.errors[0]?.message : 'Ocorreu um erro no processo de cadastro do usuário',
          'danger');
      });
    }
  }

  private async presentToast(message: string, color?: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color,
    });
    toast.onDidDismiss().then(() => {
      if (color === 'success') {
        this.router.navigateByUrl('');
      }
    });

    toast.present();
  }

  togglePasswordVisibility = (): void => {
    this.showPassword = !this.showPassword;
  }

  toggleConfirmPasswordVisibility = (): void => {
    this.showConfirmPassword = !this.showConfirmPassword;
  }
}
