import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {

  constructor(
    private router: Router
  ) { }

  goBack = (): void => {
    this.router.navigateByUrl('public/login');
  }

  navigate = (type: string): void => {
    this.router.navigate(['public/register/register-page', { type }]);
  }
}
