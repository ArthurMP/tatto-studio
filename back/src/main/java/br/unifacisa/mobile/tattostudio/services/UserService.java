package br.unifacisa.mobile.tattostudio.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.unifacisa.mobile.tattostudio.config.BCryptPasswordEncoderBean;
import br.unifacisa.mobile.tattostudio.domains.Auth;
import br.unifacisa.mobile.tattostudio.domains.User;
import br.unifacisa.mobile.tattostudio.dto.UserDTO;
import br.unifacisa.mobile.tattostudio.exceptions.BadRequestException;
import br.unifacisa.mobile.tattostudio.repositories.AuthRepository;
import br.unifacisa.mobile.tattostudio.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRep;

	@Autowired
	private AuthRepository authRep;

	@Autowired
	private BCryptPasswordEncoderBean pwEncoderBean;

	public User create(UserDTO userDTO) {
		Auth auth = new Auth();
		User user = new User();
		auth.setUsername(userDTO.getEmail());
		Optional<Auth> authUser = authRep.findByUsername(userDTO.getEmail());
		if (authUser.isPresent()) {
			throw new BadRequestException("Já existe um usuário com o username: " + userDTO.getEmail());
		}
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setPhone(userDTO.getPhone());
		auth.setUsername(userDTO.getEmail());
		auth.setRule(userDTO.getType());
		auth.setPassword(pwEncoderBean.passwordEncoder().encode(userDTO.getPassword()));
		user.setAuth(auth);
		return userRep.save(user);

	}

	public User update(Long id, UserDTO userDTO) {
		User user = this.getUserById(id);
		Optional<Auth> authUser = authRep.findByUsername(userDTO.getEmail());
		if (authUser.isPresent()) {
			throw new BadRequestException("Já existe um usuário com o username: " + userDTO.getEmail());
		}
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setPhone(userDTO.getPhone());
		user.getAuth().setUsername(userDTO.getEmail());
		user.getAuth().setPassword(pwEncoderBean.passwordEncoder().encode(userDTO.getPassword()));
		return userRep.save(user);
	}

	public void delete(Long id) {
		userRep.deleteById(id);
	}

	public List<User> getAll() {
		return userRep.findAll();
	}

	@Override
	public Auth loadUserByUsername(String username) {
		return authRep.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(
				"Não foi possível encontrar nenhum usuário com o seguinte username: " + username));
	}

	public Auth getAuthByUserId(Long id) {
		return authRep.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("Não foi possível encontrar o login"));
	}

	public User getUserById(Long userId) {
		return userRep.findById(userId).orElseThrow(() -> new BadRequestException("Usuário não encontrado"));
	}

}
