package br.unifacisa.mobile.tattostudio.exceptions;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {

	private String message;
	
	private int code;
	
	private String status;
	
	private String objectName;
	
	private List<ObjectError> errors;

	public ErrorResponse(int code, List<ObjectError> errors) {
		this.code = code;
		this.errors = errors;
	}
	
	
}
