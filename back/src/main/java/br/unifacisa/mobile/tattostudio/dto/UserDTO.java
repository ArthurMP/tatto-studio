package br.unifacisa.mobile.tattostudio.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.unifacisa.mobile.tattostudio.enums.Roles;
import lombok.Data;

@Data
public class UserDTO {

	@NotBlank
	private String firstName;
	
	@NotBlank
	private String lastName;

	@NotBlank
	private String phone;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private String password;

	@NotNull
	private Roles type;

}
