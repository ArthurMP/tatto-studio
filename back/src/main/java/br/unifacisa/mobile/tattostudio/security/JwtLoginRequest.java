package br.unifacisa.mobile.tattostudio.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.unifacisa.mobile.tattostudio.domains.Auth;

public class JwtLoginRequest extends AbstractAuthenticationProcessingFilter {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	public JwtLoginRequest(AuthenticationManager authManager, String loginUrl) {
		super(new AntPathRequestMatcher(loginUrl));
		setAuthenticationManager(authManager);
	}

	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		Auth credentials = new ObjectMapper().readValue(request.getInputStream(),
				Auth.class);
		String username = credentials.getUsername();
		String pw = credentials.getPassword();
		Authentication authentication = getAuthenticationManager().authenticate(
				new UsernamePasswordAuthenticationToken(username, pw));
		return authentication;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain,
			Authentication auth) throws IOException {
		Auth user = (Auth) auth.getPrincipal();
		jwtTokenUtil.addAuthentication(response, user);
	}
}
