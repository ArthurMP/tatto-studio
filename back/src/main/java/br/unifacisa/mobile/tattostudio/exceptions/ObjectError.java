package br.unifacisa.mobile.tattostudio.exceptions;

import lombok.Data;

@Data
public class ObjectError {
	
	private final String message;

	private final String field;
	
	private final Object parameter;
}

