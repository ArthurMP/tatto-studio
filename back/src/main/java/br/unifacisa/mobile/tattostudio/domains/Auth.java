package br.unifacisa.mobile.tattostudio.domains;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Transient;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.unifacisa.mobile.tattostudio.enums.Roles;
import lombok.Data;

@Data
@Entity
public class Auth implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "{object.not.blank}")
	@Column(unique = true)
	private String username;

	@NotBlank(message = "{object.not.blank}")
	private String password;

	private Roles rule;

	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;

	@JsonIgnore
	@Transient
	public Collection<Roles> getAuthorities() {
		return Arrays.asList(this.rule);
	}
}
