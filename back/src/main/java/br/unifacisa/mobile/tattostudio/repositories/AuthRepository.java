package br.unifacisa.mobile.tattostudio.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.unifacisa.mobile.tattostudio.domains.Auth;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long>{
	
	Optional<Auth> findByUsername(String username);
	
	boolean existsAuthByUsername(String username);
	
	

}
