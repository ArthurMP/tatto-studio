package br.unifacisa.mobile.tattostudio.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.unifacisa.mobile.tattostudio.domains.User;
import br.unifacisa.mobile.tattostudio.dto.UserDTO;
import br.unifacisa.mobile.tattostudio.services.UserService;

@RestController
@RequestMapping("public/signup")
public class SignUpController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<User> create(@Valid @RequestBody UserDTO userDTO) {
		return new ResponseEntity<>(userService.create(userDTO), HttpStatus.OK);
	}

}
