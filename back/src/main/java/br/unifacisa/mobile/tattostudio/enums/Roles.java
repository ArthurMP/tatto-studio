package br.unifacisa.mobile.tattostudio.enums;

import org.springframework.security.core.GrantedAuthority;

public enum Roles implements GrantedAuthority {

	ADMIN, CLIENT, EMPLOYEE;

	public String getAuthority() {
		return this.toString();
	}

}
