package br.unifacisa.mobile.tattostudio.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BCryptPasswordEncoderBean {

	@Value("${pw.encode.length}")
	private String length;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(Byte.parseByte(length));
	}

}