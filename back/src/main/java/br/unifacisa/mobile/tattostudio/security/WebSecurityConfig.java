package br.unifacisa.mobile.tattostudio.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;

import br.unifacisa.mobile.tattostudio.config.BCryptPasswordEncoderBean;
import br.unifacisa.mobile.tattostudio.config.CorsConfig;
import br.unifacisa.mobile.tattostudio.filters.JwtAuthFilter;
import br.unifacisa.mobile.tattostudio.services.UserService;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${login.url}")
	private String loginUrl;

	@Value("${server.servlet.context-path}")
	private String basePath;

	@Autowired
	private CorsConfig corsConfig;

	@Autowired
	private JwtAuthEntryPoint unauthorizedHandler;

	@Autowired
	private UserService userDetailsService;

	@Autowired
	private BCryptPasswordEncoderBean encoderBean;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().cors().configurationSource(corsConfig.getCorsConfiguration()).and().authorizeRequests()
				.antMatchers("/public/**", "/v2/api-docs", "/configuration/ui", "/swagger-resources",
						"/swagger-resources/**", "/configuration/security", "/swagger-ui.html", "api/", "/webjars/**")
				.permitAll().anyRequest().authenticated().and().exceptionHandling()
				.authenticationEntryPoint(unauthorizedHandler).and()
				.addFilterBefore(jwtLoginRequestBean(), UsernamePasswordAuthenticationFilter.class).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.addFilterBefore(jwtAuthFilterBean(), UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoderBean.passwordEncoder());
	}

	@Bean
	public JwtLoginRequest jwtLoginRequestBean() throws Exception {
		return new JwtLoginRequest(authenticationManager(), loginUrl);
	}

	@Bean
	public JwtAuthFilter jwtAuthFilterBean() {
		return new JwtAuthFilter();
	}

	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON).favorPathExtension(true);
	}

}