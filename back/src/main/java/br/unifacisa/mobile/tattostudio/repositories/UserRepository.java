package br.unifacisa.mobile.tattostudio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.unifacisa.mobile.tattostudio.domains.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByAuthId(Long id);

}