package br.unifacisa.mobile.tattostudio.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import br.unifacisa.mobile.tattostudio.domains.Auth;
import br.unifacisa.mobile.tattostudio.security.JwtTokenUtil;
import br.unifacisa.mobile.tattostudio.services.UserService;



public class JwtAuthFilter extends GenericFilterBean {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService personServ;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;

		String jwt = jwtTokenUtil.getTokenFromRequest(req);
		if (jwt != null) {

			jwtTokenUtil.validateToken(jwt);
			Long id = Long.valueOf(jwtTokenUtil.getUserKeyFromJWT(jwt));

			Auth userDetails = personServ.getAuthByUserId(id);

			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
					null, userDetails.getAuthorities());

			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

			SecurityContextHolder.getContext().setAuthentication(authentication);
		
		}
		
		filterChain.doFilter(request, response); 

	}
	
}