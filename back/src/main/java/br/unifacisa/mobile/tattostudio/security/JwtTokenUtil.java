package br.unifacisa.mobile.tattostudio.security;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.unifacisa.mobile.tattostudio.domains.Auth;
import br.unifacisa.mobile.tattostudio.exceptions.InvalidTokenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenUtil {

	@Value("${jwt.secret}")
	private String jwtSecret;

	@Value("${jwt.expirationInMs}")
	private int jwtExpirationInMs;

	@Value("${header.variables.token.name}")
	private String tokenName;

	@Value("${header.variables.token.prefix}")
	private String tokenPrefix;

	public void addAuthentication(HttpServletResponse response, Auth auth) throws IOException {
		String JWT = generateToken(auth);
		response.addHeader(tokenName, tokenPrefix + " " + JWT);
		response.setHeader("Access-Control-Expose-Headers", tokenName);
	}

	public String getUserKeyFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

	public String getTokenFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader(tokenName);
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(tokenPrefix)) {
			return bearerToken.substring(tokenPrefix.length() + 1);
		}
		return null;
	}

	public boolean validateToken(String authToken) {
		String message = "";
		try {
			Jwts.parser().setSigningKey(jwtSecret.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(authToken);
			return true;
		} catch (MalformedJwtException ex) {
			message = "Invalid token";
		} catch (ExpiredJwtException ex) {
			message = "Expired token";
		} catch (UnsupportedJwtException ex) {
			message = "Unsupported token";
		} catch (IllegalArgumentException ex) {
			message = "Claims string is empty.";
		}
		throw new InvalidTokenException(message);
	}

	private String generateToken(Auth auth) {
		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", auth.getAuthorities());
		
		return Jwts.builder().setSubject(auth.getId().toString()).setIssuedAt(now).setExpiration(expiryDate).addClaims(claims)
				.signWith(SignatureAlgorithm.HS512, jwtSecret.getBytes(StandardCharsets.UTF_8)).compact();
	}
	
}