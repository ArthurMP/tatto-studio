package br.unifacisa.mobile.tattostudio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4369051432148132807L;
	
	public BadRequestException(String message) {
		super(message);
	}
	
	
	
	

}
